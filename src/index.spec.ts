import plugin from "./index";

describe("Sample test", () => {
  it("Increments the argument", () => {
    expect(plugin(3)).toBe(4);
  });
});
