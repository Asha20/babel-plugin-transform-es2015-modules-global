import typescript from "rollup-plugin-typescript2";

export default {
  input: "src/index.ts",
  output: [
    {
      file: "dist/index.esm.js",
      format: "esm",
      sourcemap: true,
    },
    {
      file: "dist/index.js",
      name: "BabelTransformES2015ModulesGlobal",
      format: "iife",
      sourcemap: true,
    },
  ],

  plugins: [typescript()],
};
