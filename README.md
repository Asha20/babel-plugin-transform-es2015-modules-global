# babel-plugin-transform-es2015-modules-global

A Babel plugin that makes ESM exports store properties in a global object and makes imports pull values from global objects.